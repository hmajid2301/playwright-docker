FROM ubuntu:22.04

RUN apt-get update -y && \
    apt-get install -y libglib2.0-0\                     
    curl \
    libnss3\                                     
    libnspr4\                                    
    libatk1.0-0\                                 
    libatk-bridge2.0-0\                          
    libcups2\                                    
    libdrm2\                                     
    libdbus-1-3\                                 
    libxcb1\                                     
    libxkbcommon0\                               
    libx11-6\                                    
    libxcomposite1\                              
    libxdamage1\                                 
    libxext6\                                    
    libxfixes3\                                  
    libxrandr2\                                  
    libgbm1\                                     
    libpango-1.0-0\                              
    libcairo2\                                   
    libasound2\                                  
    libatspi2.0-0\                               
    libwayland-client0  && \
    curl -sL https://deb.nodesource.com/setup_19.x | bash - && \ 
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && \
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" |  tee /etc/apt/sources.list.d/docker.list > /dev/null && \
    apt-get remove -y --purge cmdtest && \
    apt-get update && \
    apt-get install -y nodejs docker-ce docker-compose-plugin && \
    node --version && \
    npm --version && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /var/lib/apt/lists.d/* && \
    apt-get autoremove && \
    apt-get clean && \
    apt-get autoclean && \
    npx playwright install-deps && \
    curl -fsSL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --yes --dearmor -o /usr/share/keyrings/yarnkey.gpg && \
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list > /dev/null && \
    npm install playwright-chromium @playwright/test pnpm @go-task/cli -g

CMD ["node"]


