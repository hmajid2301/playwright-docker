# playwright-docker

Uses playwright microsft Docker image as a base. It adds docker and node 18 to the base image.
This image is intended for use with Gitlab CI. So we can spin up containers using docker compose
in the job itself.

## Usage

Example in `gitlab-ci.yml` 

```yml
tests:e2e:
  stage: test
  only:
    - merge_request
  image: registry.gitlab.com/hmajid2301/playwright-docker:latest
  services:
    - name: docker:dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
  extends: .task
  script:
    - npx playwright install
    - playwright test
  artifacts:
    when: always
    paths:
      - test-results/
    expire_in: 1 week
```